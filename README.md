# Personal Website

This project contains the source code for my website [utkarshsingh.xyz](https://utkarshsingh.xyz).

## Dependencies 

- Angular
- ngx-markdown
- angular-fontawesome
- prims.js

## License

All original text, such as blog posts is available under [CC BY-SA 4.0
International](https://creativecommons.org/licenses/by-sa/4.0/legalcode)
and all original code is available under [GPL 3 or
later](https://www.gnu.org/licenses/gpl-3.0.html).
