import { Component, OnInit } from '@angular/core';
import {
  IconDefinition,
  faGithub,
  faGitlab,
  faLinkedin,
} from '@fortawesome/free-brands-svg-icons';
import { faBook, faCode } from '@fortawesome/free-solid-svg-icons';

export interface Links {
  icon: IconDefinition;
  iconName: string;
  url: string;
  topic: string;
}

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css'],
})
export class AboutComponent implements OnInit {
  faGitlab = faGitlab;
  faGithub = faGithub;
  faLinkedin = faLinkedin;
  faCode = faCode;
  faBook = faBook;
  links: Links[] = [];

  ngOnInit(): void {
    this.links = [
      {
        icon: faGitlab,
        iconName: 'Gitlab',
        url: 'https://gitlab.com/utkarsh181',
        topic: 'Software Projects',
      },
      {
        icon: faGithub,
        iconName: 'Github',
        url: 'https://github.com/utkarsh181',
        topic: 'Open Source Project Collaboration',
      },
      {
        icon: faLinkedin,
        iconName: 'LinkedIn',
        url: 'https://www.linkedin.com/in/utkarsh---singh',
        topic: 'Business and Employment',
      },
      {
        icon: faCode,
        iconName: 'LeetCode',
        url: 'https://leetcode.com/utkarsh-singh',
        topic: 'Algorithmic Problems',
      },
      {
        icon: faBook,
        iconName: 'Open Library',
        url: 'https://openlibrary.org/people/utkarsh181/books',
        topic: 'Books read',
      },
    ];
  }
}
