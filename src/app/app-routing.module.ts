import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { Routes } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { BlogPostListComponent } from './blog/blog-post-list/blog-post-list.component';
import { BlogPostComponent } from './blog/blog-post/blog-post.component';
import { IntroComponent } from './intro/intro.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

export const routes: Routes = [
  { path: '', pathMatch: 'full', component: IntroComponent },
  { path: 'about', component: AboutComponent },
  { path: 'blog', component: BlogPostListComponent },
  { path: 'blog/:id', pathMatch: 'full', component: BlogPostComponent },
  { path: '404', component: PageNotFoundComponent },
  { path: '**', redirectTo: '/404' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
