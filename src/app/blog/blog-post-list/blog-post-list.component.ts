import { Component, OnInit } from '@angular/core';
import { BlogPost, blogPostList } from 'src/config/blog-post-list';

@Component({
  selector: 'app-blog-post-list',
  templateUrl: './blog-post-list.component.html',
  styleUrls: ['./blog-post-list.component.css'],
})
export class BlogPostListComponent implements OnInit {
  blogPostList: BlogPost[] = [];

  // Create a 'slug', which will be the url to the post
  slugify(text: string): string {
    return text
      .toString()
      .toLowerCase()
      .replace(/\s+/g, '-') // Replace spaces with -
      .replace(/[^\w\-]+/g, '') // Remove all non-word chars
      .replace(/\-\-+/g, '-') // Replace multiple - with single -
      .replace(/^-+/, '') // Trim - from start of text
      .replace(/-+$/, ''); // Trim - from end of text
  }

  ngOnInit(): void {
    // Add the slug to the blogPost
    this.blogPostList = blogPostList.map((blogPost) => ({
      ...blogPost,
      slug: this.slugify(blogPost.title),
    }));
  }
}
