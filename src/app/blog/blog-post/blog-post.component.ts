import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, map } from 'rxjs';

@Component({
  selector: 'app-blog-post',
  templateUrl: './blog-post.component.html',
  styleUrls: ['./blog-post.component.css'],
})
export class BlogPostComponent implements OnInit {
  blogPost$: Observable<string> | undefined;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
  ) {}

  ngOnInit(): void {
    this.blogPost$ = this.route.params.pipe(
      map((params) => `/assets/${params['id']}/${params['id']}.md`),
    );
  }

  handleError(errorRes: string | Error) {
    this.router.navigate(['/404']);
  }
}
