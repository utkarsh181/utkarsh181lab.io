export interface BlogPost {
  title: string;
  subTitle?: string;
  slug?: string;
  date: Date;
}

export const blogPostList: BlogPost[] = [
  {
    title: 'Thinkpad P14s',
    subTitle: 'A Linux friendly, powerful and portable laptop',
    date: new Date('2024-01-01'),
  },
  {
    title: 'Integer Arithmetic',
    subTitle: 'Getting help form hardware for overflow and underflow',
    date: new Date('2022-06-12'),
  },
  {
    title: 'Symbolic Differentiation with Lisp',
    subTitle: 'Now, doing your Maths homework is fun and Lispy!',
    date: new Date('2021-08-19'),
  },
  {
    title: 'Book review: "Programming Principles And Practice Using C++"',
    subTitle: 'Learning C++ from its inventor',
    date: new Date('2021-07-13'),
  },
  {
    title: 'Shell scripts are irreplaceable!',
    subTitle: 'Scripting in Sh, Python and Elisp',
    date: new Date('2021-04-11'),
  },
  {
    title: 'Timetable with GNU Emacs',
    subTitle: 'Manage your timetable as plaintext',
    date: new Date('2021-03-02'),
  },
];
