# Time Table with GNU Emacs

*Manage your timetable as plaintext.*

## Introduction

It's not a very well kept secret that Emacs is an extensible editor
and can be even extended to manage your university timetable.  My
university distributes time table in MS (Micorsoft) Excel file format,
which are pain to work with.  So, I decided to use some Elisp
facilities to filter, accumulate and interact with timetable's data.


## XML Escape

In order to get started, we have to first export all our data into
Comma Separated Value (CSV) file format.  MS Excel files can be easily
converted to CSV file by a word processor such as [Libreoffice](https://www.libreoffice.org/).

Start by opening your timetable file with Libreoffice and by pressing
`C-s` you will be prompted with a dialog, choose CSV as file extension
and then exit your Libreoffice session.

![img](csv.png "Libreoffice Save Dialog")


## Importing CSV file

Out the box Emacs comes with several methods to import and display CSV
file as plain text tables.  But in this case, we will make a subtle
combination of importing and filtering as the file I am going to
import contains timetable for all the available batches.

Here is the section presenting all the class scheduled for Monday:

```
Mon,L-18B11BI312 BI41 (RST) CR9,L-18B11CI415 BI41 (MBI) TR1,L-18B11BT313 BT41 (PNS) CR11,
,"L-18B11CI413 CS41-CS48,IT41-42 (RSU) TR1","L-18B11MA313 CS41-CS48,IT41-42 (SST) ECL9",
,,,P-18B17EC373 EC42 (MSD) ECL8,P-18B17EC373 EC42 (MSD) ECL8,,P-18B17EC371 EC41 (EMP) CL10,P-18B17EC371 EC41 (EMP) CL10,,
,,,P-18B17CI372 IT41 (PDG) CL1,P-18B17CI372 IT41 (PDG) CL1,,P-18B17BI473 BI41 (RJK) BIL,P-18B17BI473 BI41 (RJK) BIL,,
,,,P-18B17CI372 CS48 (NFK) CL3_1,P-18B17CI372 CS48 (NFK) CL3_1,,P-18B11CE413 CE41 (TNM) WORK2,P-18B11CE413 CE41 (TNM) WORK2,,
,,,P-18B17CI372 IT42 (PKJ) CL3_2,P-18B17CI372 IT42 (PKJ) CL3_2,,,P-18B17BT373 BT41 (PNS) UG2,P-18B17BT373 BT41 (PNS) UG2,
,,,P-18B17CI473 CS46 (RSU) CL4,P-18B17CI473 CS46 (RSU) CL4,,,,P-18B17CI372 CS42 (SLD) CL1,P-18B17CI372 CS42 (SLD) CL1
,,,P-18B17CI373 CS41 (HRI) CL5_1,P-18B17CI373 CS41 (HRI) CL5_1,,,,P-18B17CI372 CS41 (VKB) CL3_1,P-18B17CI372 CS41 (VKB) CL3_1
,,,P-18B17CI373 CS42 (PTK) CL5_2,P-18B17CI373 CS42 (PTK) CL5_2,,,,P-18B17CI473 CS44 (PKG) CL3_2,P-18B17CI473 CS44 (PKG) CL3_2
,,,P-18B17CI373 CS43 (EGA) CL6,P-18B17CI373 CS43 (EGA) CL6,,,,P-18B17CI473 CS43 (RKI) CL4,P-18B17CI473 CS43 (RKI) CL4
,,,P-18B17CI373 CS44 (HES) CL7,P-18B17CI373 CS44 (HES) CL7,,,,P-18B17CI373 CS46 (PMI) CL5_1,P-18B17CI373 CS46 (PMI) CL5_1
,,,P-18B17CI373 CS45 (MBI) CL8,P-18B17CI373 CS45 (MBI) CL8,,,,P-18B17CI373 CS47 (AMN) CL5_2,P-18B17CI373 CS47 (AMN) CL5_2
,,,P-18B17CI473 CS47 (MSH) CL9_1,P-18B17CI473 CS47 (MSH) CL9_1,,,,P-18B17CI373 CS48 (JAG) CL6,P-18B17CI373 CS48 (JAG) CL6
,,,P-18B11CI474 BI41 (SJT) CL11,P-18B11CI474 BI41 (SJT) CL11,,,,P-18B17CI373 IT41 (HJL) CL7,P-18B17CI373 IT41 (HJL) CL7
,,,P-18B17CE472 CE41 (SUB) ENVLAB,P-18B17CE472 CE41 (SUB) ENVLAB,,,,P-18B17CI373 IT42 (AKJ) CL8,P-18B17CI373 IT42 (AKJ) CL8
,,,,,,,,P-18B17CI473 CS45 (ARV) CL9_1,P-18B17CI473 CS45 (ARV) CL9_1
```

## ttable.el

Ttable is timetable editing utilies for Org mode.  It takes CSV file
and current batch as inputs and output Org mode tables.  Here is a
screenshot:

![img](time-table.png "Org-mode timetable")

Ttable source code: [ttable.el](https://gitlab.com/utkarsh181/dotfiles/-/raw/main/emacs/.config/emacs/lisp/ttable.el)


## Conclusion

This article was written to emphasize on Emacs biggest feature:
**extensibility**.  Emacs elegantly extended itself to present CSV files
as spreadsheet-like tables.  In the process, I also learned how the
choice of an optimal data structure can make big impact on your code.

