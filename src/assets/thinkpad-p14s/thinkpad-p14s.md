# The ThinkPad P14s

*A Linux friendly, powerful and portable laptop*

## Introduction

![img](cover.jpg)

For the past year, I have been searching for a replacement for my
Lenovo Ideapad. Its 8 GB RAM couldn't keep up with the bloated
software I run these days. Additionally, the palm rest has developed
strange marks from prolonged usage during debugging sessions in the
hot and humid climate of Delhi. The final straw was a
[suspension issue](https://bbs.archlinux.org/viewtopic.php?id=276987),
possibly introduced due to a regression in the Linux kernel

## Requirements

My requirements were quite specific, requiring a significant amount of
effort on my part:

- **Linux**: It's 2024, the year of desktop Linux!
- **Weight**: I was tired of carrying around 2.2-kilograms Ideapad.
- **Cost**: Anything above ₹130k (≍ $1600) was hardly justifiable.
- **RAM**: As earlier stated, I need to run various software for
  development-related tasks, and anything below 16 GB was not
  future-proof.

## Contenders

- **Asus X13 Flow**: [icyphox](https://icyphox.sh/blog/flow-x13/)
  loves it and it ticks all the boxes, but it comes with a price tag
  of around 180k INR (≍2100 USD).
- **Macbook M2 Air**: Overall it's a good laptop, equipped with great
  CPU, screen and speakers. Additionally, [Asahi
  Linux](https://asahilinux.org/) has come a long way, but I still
  have doubts about the stability of Linux in ARM desktop space.

## Hardware

I have opted to buy Gen 4, which comes with a powerful AMD Ryzen 7 PRO
7840U and 32 GB of RAM. To benchmark this, I decided to compile Emacs
from scratch:

```
$ time MAKEFLAGS="-j$(nproc)" makepkg -sCi
==> Making package: emacs-git 30.0.50.170220-1 (Mon 01 Jan 2024 07:46:27 PM IST)

real  3m19.196s
user  20m19.787s
sys   2m12.983s
```

And unsurprisingly, its 10x faster than my Ideapad's i5 8250U.

|                   |                    |
|-------------------|--------------------|
|![img](left.jpg) | ![img](right.jpg)    |


Additionally, it comes with a superb port selection. On the left side,
you get Ethernet, 2 USB-C, HDMI, USB-A, and a 3.5mm
headphone/microphone jack. On the right, there's a USB-A port and a
smart card reader. The presence of 2 USB-A ports allows me to connect
a keyboard and mouse without needing a dongle.

Finally, here’s the full spec list:

- Ryzen 7 7840U, 8 cores & 16 threads
- 32 GB LPDDR5X RAM @ 6400 MT/s
- 14" WUXGA (1920x1200) IPS, low power display
- 52.5 Wh battery
- 1 TB M.2 SSD

## Software

I have decided to go with the boring Arch Linux setup running
GNOME. Everything works out the box but its
[ArchWiki](https://wiki.archlinux.org/title/Lenovo_ThinkPad_T14_(AMD)_Gen_4)
entry is a must read.

Initially, the sound quality from Speakers was inferior compared to
Windows 11, but with EasyEffect +
[presets](https://stuff.kurz.pw/arch/P14s_G4/Speakers/), they are
enjoyable again:

![img](easy-effects.png)

Currently, power-profiles-daemon used with GNOME doesn't support AMD
P-State EPP and I'm not able to switch power profiles from power-saver
to balanced:

```
$ powerprofilesctl get
power-saver
$ powerprofilesctl set balanced
$ powerprofilesctl get
power-saver
```

Although, I'm not sure it's because of P-State EPP, but it surely requires a [Linux
Evening](https://fabiensanglard.net/a_linux_evening/index.html).

## Conclusion

Overall, I'm really happy with this machine. Reasonably priced,
future-proof and checks all the boxes. You can find my configuration
[here](https://gitlab.com/utkarsh181/dotfiles).
